package no.noroff.northwindthymeleaf.repositories;

import no.noroff.northwindthymeleaf.models.Customer;
import no.noroff.northwindthymeleaf.util.ConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final ConnectionFactory connectionFactory;

    public CustomerRepositoryImpl(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public List<Customer> getAll() {
        List<Customer> returnCustomers = new ArrayList<>();
        try(Connection conn = connectionFactory.getConnection()) {
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Id,CompanyName, ContactName, City FROM customer");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                returnCustomers.add(
                        new Customer(
                                resultSet.getString("Id"),
                                resultSet.getString("CompanyName"),
                                resultSet.getString("ContactName"),
                                resultSet.getString("City")
                        ));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnCustomers;
    }

    @Override
    public Customer getById(String id) {
        Customer returnCustomer = null;
        try(Connection conn = connectionFactory.getConnection()) {
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Id,CompanyName, ContactName, City FROM customer WHERE Id = ?");
            preparedStatement.setString(1,id);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                returnCustomer = new Customer(
                        resultSet.getString("Id"),
                        resultSet.getString("CompanyName"),
                        resultSet.getString("ContactName"),
                        resultSet.getString("City")
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnCustomer;
    }

    @Override
    public int add(Customer customer) {
        int result = 0;
        try(Connection conn = connectionFactory.getConnection()) {
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO customer(Id,CompanyName,ContactName,City) VALUES(?,?,?,?)");
            //preparedStatement.setString(1,customer.Id());
            //preparedStatement.setString(2,customer.CompanyName());
            //preparedStatement.setString(3,customer.ContactName());
            //preparedStatement.setString(4,customer.City());
            // Execute Query
            result = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int update(Customer customer) {
        return 0;
    }

    @Override
    public int delete(String id) {
        return 0;
    }
}
