package no.noroff.northwindthymeleaf.controllers;

import no.noroff.northwindthymeleaf.models.Customer;
import no.noroff.northwindthymeleaf.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("customers")
public class CustomerController {

    private CustomerRepository customerRepository;

    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @GetMapping
    public String index(Model model) {
        //List<Customer> customers = customerRepository.getAll();
        model.addAttribute("customers", customerRepository.getAll());
        return "view-customers";
    }

    @GetMapping("add")
    public String showAddCustomerView(Model model) {
        model.addAttribute("customer", new Customer());
        return "create-customer";
    }

    @PostMapping("add")
    public String addCustomer(@ModelAttribute Customer customer, BindingResult errors, Model model) {

        model.addAttribute("customer", new Customer());
        return "create-customer";
    }
}
