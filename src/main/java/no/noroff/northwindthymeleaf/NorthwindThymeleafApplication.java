package no.noroff.northwindthymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NorthwindThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(NorthwindThymeleafApplication.class, args);
    }

}
