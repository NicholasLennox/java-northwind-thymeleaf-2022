package no.noroff.northwindthymeleaf.util;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

// Tags this class as injectable by DI
@Component
// Spring will inject this class when not in production
@Profile("!production")
public class DevConnectionFactory implements ConnectionFactory {
    @Override
    public Connection getConnection() throws SQLException {
        String connectionString = "jdbc:sqlite:src/main/resources/Northwind_small.sqlite";
        return DriverManager.getConnection(connectionString);
    }
}
